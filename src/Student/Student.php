<?php
namespace App\Student;
use App\Message\Message;
use App\Utility\Utility;

class Student
{
    public $id;
    public $fullname;
    public $coursename;
    public $deleted_at;
    public $conn;

    public function __construct(){
        $this->conn=mysqli_connect("localhost","root","","labxm6b21") or die("Connection failed");;
    }

    public function prepare($data=Array()){
        if(array_key_exists("fullname",$data)){
            $data['fullname']=Utility::validation($data['fullname']);
            $this->fullname=filter_var($data['fullname'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        }
        if(array_key_exists("coursename",$data)){
            $data['coursename']=Utility::validation($data['coursename']);
            $this->coursename=filter_var($data['coursename'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        }
        if(array_key_exists("id",$data)){
            $this->id=$data['id'];
        }
        return $this;
    }
    public function index(){
        $allStudent = array();
        $query="SELECT * FROM `student_data` WHERE `deleted_at`='NULL'";
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $allStudent[] = $row;
        }
        return $allStudent;
    }

    public function store(){
        $query="INSERT INTO `student_data` (`full_name`, `course_name`,`deleted_at`) VALUES ('".$this->fullname."', '".$this->coursename."','NULL')";
        $result=mysqli_query($this->conn,$query);
        if($result>0){
            Message::message(
                "
                <div>
                    <strong>Success!</strong> Data has been changed successfully.
                </div>
                ");
            Utility::redirect('index.php');
        }
        else{
            Message::message(
                "
                   <div>
                    <strong>Error!</strong> Data has not been changed successfully.
                   </div>
                ");
            Utility::redirect('index.php');
        }
    }

    public function view(){
        $query="SELECT * FROM `student_data` WHERE `id`='".$this->id."'";
        $result=mysqli_query($this->conn,$query);
        $row=mysqli_fetch_assoc($result);
        return $row;
    }

    public function delete(){
        $query = "DELETE FROM `student_data` WHERE `id`='".$this->id."'";
        $result=mysqli_query($this->conn,$query);
        if($result>0){
            Message::message(
                "
                <div>
                    <strong>Success!</strong> Data has been Deleted successfully.
                </div>
                ");
            Utility::redirect('index.php');
        }
        else{
            Message::message(
                "
                   <div>
                    <strong>Error!</strong> Data has not been Deleted successfully.
                   </div>
                ");
            Utility::redirect('index.php');
        }
    }

    public function trash(){
        $query = "UPDATE `student_data` SET `deleted_at`='trash' WHERE `id`='".$this->id."'";
        $result=mysqli_query($this->conn,$query);
        if($result>0){
            Message::message(
                "
                <div>
                    <strong>Success!</strong> Data has been Trashed successfully.
                </div>
                ");
            Utility::redirect('index.php');
        }
        else{
            Message::message(
                "
                   <div>
                    <strong>Error!</strong> Data has not been Trashed successfully.
                   </div>
                ");
            Utility::redirect('index.php');
        }
    }

    public function viewtrash(){
        $allStudent = array();
        $query="SELECT * FROM `student_data` WHERE `deleted_at`='trash'";
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result)){
            $allStudent[] = $row;
        }
        return $allStudent;
    }

    public function recover(){
        $query = "UPDATE `student_data` SET `deleted_at`='NULL' WHERE `id`='".$this->id."'";
        $result=mysqli_query($this->conn,$query);
        if($result>0){
            Message::message(
                "
                <div>
                    <strong>Success!</strong> Data has been Trashed successfully.
                </div>
                ");
            Utility::redirect('index.php');
        }
        else{
            Message::message(
                "
                   <div>
                    <strong>Error!</strong> Data has not been Trashed successfully.
                   </div>
                ");
            Utility::redirect('index.php');
        }
    }

}