<?php
namespace App\Message;


class Message
{
    public static function message($message=NULL){
        if(is_NULL($message)){
            $message=self::getMessage();
            return $message;
        }
        else{
            self::setMessage($message);
        }
    }

    public static function setMessage($message){
        $_SESSION['message']=$message;
    }

    public static function getMessage(){
        $message=$_SESSION['message'];
        $_SESSION['message']=NULL;
        return $message;
    }
}