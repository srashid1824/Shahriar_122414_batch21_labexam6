<?php
namespace App\Utility;


class Utility
{
    public static function validation($data=NULL){
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
    public static function redirect($data=NULL){
        header('Location:'.$data);
    }
}