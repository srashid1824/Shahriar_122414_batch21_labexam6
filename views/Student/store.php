<?php
include_once('../../vendor/autoload.php');
use App\Student\Student;
if((isset($_POST['fullname']) && !empty($_POST['fullname'])) && (isset($_POST['coursename']) && !empty($_POST['coursename']))){
    $fullname = $_POST['fullname'];
    $coursename=$_POST['coursename'];
    $comma_seperated_data=implode(",",$coursename);
    $_POST['coursename']=$comma_seperated_data;
    $storeStudent=new Student();
    $storeStudent->prepare($_POST)->store();
}
else{
    echo "Error insert data";
}