<?php
include_once('../../vendor/autoload.php');
use App\Message\Message;
use App\Student\Student;
session_start();
$allStudent=new Student();
$allStudent=$allStudent->viewtrash();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>BITM Lab Exam 6</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../resources/bootstrap/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="../../resources/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Lab Exam 6</h2>
    <div class="col-md-6" style="margin-bottom:15px;">
        <a href="create.php" class="btn btn-success" role="button">Add New Student</a>
        <a href="index.php" class="btn btn-danger" role="button">Back to index</a>
    </div>
    <div id="message">
        <?php
        if(array_key_exists("message",$_SESSION) && !empty($_SESSION['message'])){
            echo Message::message();
        }
        ?>
    </div>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>SL</th>
            <th>ID</th>
            <th>Name</th>
            <th>Hobby</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $sl=0;
        foreach($allStudent as $student) {
            $sl++;
            ?>
            <tr>
                <td><?php echo $sl;?></td>
                <td><?php echo $student['id'];?></td>
                <td><?php echo $student['full_name'];?></td>
                <td><?php echo $student['course_name'];?></td>
                <td>
                    <a href="recover.php?id=<?php echo $student['id'];?>" class="btn btn-primary" role="button">Recover</a>

                </td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
</div>

</body>
</html>

