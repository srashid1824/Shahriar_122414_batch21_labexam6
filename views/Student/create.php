<!DOCTYPE html>
<html lang="en">
<head>
    <title>Student</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../resources/bootstrap/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="../../resources/js/bootstrap.min.js"></script>
    <style>
        ul li{
            list-style: none;
        }
    </style>
</head>
<body>

<div class="container">
    <h2>Create a New Student</h2>
    <form name="create" action="store.php" method="post">
        <div class="form-group">
            <div class="col-md-12">
                <label for="fullname">Name:</label>
                <input type="text" class="form-control" name="fullname" required>
            </div>
            <div class="col-md-12" style="margin-top:10px;">
                <label>Select your course</label>
                <ul>
                    <li><label><input type="checkbox" name="coursename[]" value="PHP">&nbsp;PHP</label></li>
                    <li><label><input type="checkbox" name="coursename[]" value="Java">&nbsp;Java</label></li>
                    <li><label><input type="checkbox" name="coursename[]" value="Python">&nbsp;Python</label></li>
                    <li><label><input type="checkbox" name="coursename[]" value="DotNet">&nbsp;DotNet</label></li>
                    <li><label><input type="checkbox" name="coursename[]" value="Oracle">&nbsp;Oracle</label></li>
                    <li><label><input type="checkbox" name="coursename[]" value="HTML">&nbsp;HTML</label></li>
                </ul>
            </div>

        </div>
        <input type="submit" class="btn btn-success" value="Save" style="margin-top:10px;"/>
    </form>
</div>
<script>
    $('#message').show().delay(3000).fadeOut();
</script>
</body>
</html>

