<?php
include_once('../../vendor/autoload.php');
use App\Student\Student;

$viewStudent=new Student();
$singleStudent=$viewStudent->prepare($_GET)->view();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Student</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../resources/bootstrap/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="../../resources/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Student</h2>
    <div class="col-md-6" style="margin-bottom:15px;">
        <a href="index.php" class="btn btn-primary" role="button">All Students</a>
        <a href="create.php" class="btn btn-success" role="button">Add New Student</a>
        <a href="view-trash.php" class="btn btn-danger" role="button">Trash List</a>
    </div>
</div>
<div class="container">
    <p>Student Name: &nbsp;<?php echo $singleStudent['id'];?></p>
    <p>Student Name: &nbsp;<?php echo $singleStudent['full_name'];?></p>
    <p>Favourite Courses: &nbsp;<?php echo $singleStudent['course_name'];?></p>
</div>

</body>
</html>

